package lab.librari.service;

import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BrowsingServiceStarter {

    public static void main(String[] args) {
        System.out.println("BrowsingServiceStarter.main");

        ApplicationContext context =
                new AnnotationConfigApplicationContext(LibrariConfiguration.class);

        BrowsingService bs = context.getBean(BrowsingService.class);
        
        Publisher p = bs.getPublisher(5L);

        System.out.println("p = " + p);
        
    }
}

package lab.librari.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("lab.librari")
public class LibrariConfiguration {
}

package lab.librari.dao.impl.jdbc;

import lab.librari.model.Publisher;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PublisherMapper implements RowMapper<Publisher> {

    @Override
    public Publisher mapRow(ResultSet resultSet, int i) throws SQLException {
        Publisher r = new Publisher();
        r.setId(resultSet.getLong("publisher_id"));
        r.setName(resultSet.getString("publisher_name"));
        r.setLogoImage(resultSet.getString("publisher_logo"));
        return r;
    }
}

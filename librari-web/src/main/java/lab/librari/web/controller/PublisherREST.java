package lab.librari.web.controller;

import lab.librari.model.Book;
import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class PublisherREST {


    Logger logger = Logger.getLogger(PublisherREST.class.getName());

    @Autowired
    BrowsingService bs;

    @GetMapping("/publishers")
    public List<Publisher> getPublishers(){
        logger.info("about to fetch publishers list");
        return bs.getPublishers();
    }

    @GetMapping(value = "/publishers/{pid}/books", produces = "*/*")
    public List<Book> getBooks(@PathVariable("pid") long publisherId){
        logger.info("fetching books of publisher " + publisherId);
        return bs.getBooksForPublisher(publisherId);
    }

    @PostMapping("/publishers/{pid}/books")
    public ResponseEntity<Book> addBook(
            @PathVariable("pid") long publisherId,
            @RequestBody Book b){
        logger.info("about to add book " + b + " to publisher " + publisherId);

        return new ResponseEntity<>(
                bs.addBook(bs.getPublisher(publisherId), b),
                HttpStatus.CREATED
        );

    }


}

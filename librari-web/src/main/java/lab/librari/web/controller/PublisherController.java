package lab.librari.web.controller;

import lab.librari.model.Book;
import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@Controller
@SessionAttributes("publisher")
public class PublisherController {

    private Logger logger = Logger.getLogger(PublisherController.class.getName());

    @Autowired
    private BrowsingService bs;

    @Autowired
    private MessageSource ms;

    //@RequestMapping(value = "/publishers", method = RequestMethod.GET)
    @GetMapping("/publishers")
    public String getPublishers(Model model, Locale locale){
       logger.info("about to fetch publishers list");
       List<Publisher> publishers = bs.getPublishers();

       model.addAttribute("publishers", publishers);
       model.addAttribute("title",
               ms.getMessage(
                       "publisher.list.title",
                       null,
                        locale));

       return "publishers";
    }

    //@RequestMapping(value = "/books", method = RequestMethod.GET)
    @GetMapping("/books")
    public String getBooks(
            HttpServletResponse resp,
            Model model,
            Locale locale,
            @RequestParam("publisherId") long publisherId,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId ){
        logger.info("about to fetch books of publisher " + publisherId);
        logger.info("user agent: " + userAgent);
        logger.info("sesson id: " + sessionId);

        List<Book> books = bs.getBooksForPublisher(publisherId);

        model.addAttribute("books", books);
        model.addAttribute("title", ms.getMessage(
                "book.list.title",
                    new Object[]{bs.getPublisher(publisherId).getName()},
                    locale));
        model.addAttribute("publisher", bs.getPublisher(publisherId));

        Cookie cookie = new Cookie("librari-cookie", "works?");
        cookie.setMaxAge(60);
        resp.addCookie(cookie);

        logger.info("language:" + locale.getDisplayLanguage(new Locale("pl", "PL")) + ", country:" + locale.getDisplayCountry(new Locale("pl", "PL")));

        return "books";
    }

    @GetMapping("/books/add")
    public String addBookPrepare(Locale locale, Model model, @RequestParam("publisherId") long publisherId){
        logger.info("Adding book for publisher " + publisherId);

        Publisher p = bs.getPublisher(publisherId);
        model.addAttribute("publisher", p);
        Book b = new Book();
        model.addAttribute("book", b);
        model.addAttribute("title",
                ms.getMessage(
                        "book.add.title",
                        new Object[]{p.getName()},
                        locale
                        ));

        return "addBook";
    }

    @PostMapping("/books/add")
    public String addBook(@ModelAttribute("book") Book b, BindingResult br, @SessionAttribute("publisher") Publisher p){

        logger.info("Adding book " + b);
        logger.info("publisher: " + p);

        if(b.getTitle()==null || b.getTitle().trim().isEmpty()){
            br.rejectValue("title", "book.add.error.title.empty");
        }
      /*  if(b.getPrice()<0){
            br.rejectValue("price", "book.add.error.price.negative");
        }*/

        if(br.hasErrors()){
            return "addBook";
        }

        bs.addBook(p, b);

        return "redirect:/books?publisherId=" + p.getId();

    }

}

package lab.librari.web.controller;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

public class TimeInterceptor extends HandlerInterceptorAdapter {

    private int opening;

    private int closing;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int currentHour = LocalTime.now().getHour();
        if(currentHour>=opening && currentHour<closing){
            return true;
        } else {
            response.sendRedirect("http://artofmove.pl/wp-content/uploads/2018/10/Closed-Sign-300x200.jpg");
            return false;
        }
    }

    public void setOpening(int opening) {
        this.opening = opening;
    }

    public void setClosing(int closing) {
        this.closing = closing;
    }
}

package lab.librari.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CounterServlet extends HttpServlet{


    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {

        HttpSession session = req.getSession(true);

        int count = 0;
        if(session.getAttribute("count")!=null) {
            count = (Integer) session.getAttribute("count");
        }

        session.setAttribute("count", ++count);

        resp.setContentType("text/html");
        resp.getWriter().write("<html><body>"
                                + "You visited this site " + count + " times"
                                + "</body><html>");

    }
}

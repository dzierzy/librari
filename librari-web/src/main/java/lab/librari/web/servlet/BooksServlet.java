package lab.librari.web.servlet;

import lab.librari.model.Book;
import lab.librari.service.api.BrowsingService;
import lab.librari.service.impl.BrowsingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

//@WebServlet("/books")
public class BooksServlet extends HttpServlet {

    Logger logger = Logger.getLogger(BooksServlet.class.getName());

    private BrowsingService bs = new BrowsingServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long publisherId = Long.parseLong(req.getParameter("publisherId"));

        logger.info("fetching books of publisher " + publisherId);

        String publisherName = bs.getPublisher(publisherId).getName();
        List<Book> books = bs.getBooksForPublisher(publisherId);
        req.setAttribute("books", books);
        req.setAttribute("title", "Books of " + publisherName);

        req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/books.jsp").forward(req, resp);
    }
}

package lab.librari.web.servlet;

import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import lab.librari.service.impl.BrowsingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

//@WebServlet("/publishers")
public class PublishersServlet extends HttpServlet {

    Logger logger = Logger.getLogger(PublishersServlet.class.getName());

    BrowsingService bs = new BrowsingServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("about to fetch publishers list");

        List<Publisher> publishers = bs.getPublishers();
        req.setAttribute("publishers", publishers);
        req.setAttribute("title", "Publishers list");

        req.
               getServletContext().
               getRequestDispatcher("/WEB-INF/jsp/publishers.jsp").
               forward(req, resp);

    }
}

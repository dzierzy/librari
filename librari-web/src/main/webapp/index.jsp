<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<jsp:include page="/WEB-INF/jsp/header.jsp"/>

Please review our <a href="<c:url value='/publishers'/>">Publishers catalog</a>

<jsp:include page="/WEB-INF/jsp/footer.jsp"/>
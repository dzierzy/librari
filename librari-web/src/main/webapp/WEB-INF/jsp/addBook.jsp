<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <jsp:include page="header.jsp"/>

        <form:form action="add" method="post" modelAttribute="book">
        <table>

            <tr>
               <td>Title: <form:input path="title"/>
                   <form:errors path="title" cssClass="error"/>
               </td>
            </tr>
            <tr>
                <td>Author: <form:input path="author"/></td>
            </tr>
            <tr>
                <td>Cover: <form:input path="cover"/></td>
            </tr>
            <tr>
                <td>price: <form:input path="price"/>
                    <form:errors path="price"/>
                </td>
            </tr>
            <tr>
                <td><input type="submit"/> </td>
            </tr>
        </table>
        </form:form>

    <jsp:include page="footer.jsp"/>

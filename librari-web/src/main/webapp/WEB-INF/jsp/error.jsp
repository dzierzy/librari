<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <jsp:include page="header.jsp"/>

        <table>
            <tr>
                <td>
                   ${message}
                </td>
            </tr>
        </table>

    <jsp:include page="footer.jsp"/>

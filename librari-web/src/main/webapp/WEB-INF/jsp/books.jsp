<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <jsp:include page="header.jsp"/>

        <table>
            <tr>
                <th><spring:message code="book.title"/> </th>
                <th><spring:message code="book.author"/></th>
                <th><spring:message code="book.cover"/></th>
                <th><spring:message code="book.price"/></th>
                <th><spring:message code="book.details"/></th>
            </tr>
            <c:forEach items="${books}" var="b">
                <tr>
                    <td><c:out value="${b.title}"/></td>
                    <td><c:out value="${b.author}"/></td>
                    <td><img src="${b.cover}"></td>
                    <td><c:out value="${b.price}"/></td>
                    <td>
                        <c:if test="${not empty b.view}">
                            <a href="${fn:trim(b.view)}">view</a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            <tr>
                <td colspan="5">
                    <a href="<c:url value="/books/add?publisherId=${publisher.id}"/>">+</a>
                </td>
            </tr>
        </table>

    <jsp:include page="footer.jsp"/>

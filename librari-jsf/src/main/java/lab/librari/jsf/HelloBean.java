package lab.librari.jsf;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

// JavaBean == POJO = Plain Old Java Object
@ManagedBean(name="helloBean")
@ApplicationScoped
public class HelloBean {


    private int count = 0;

    public String getMessage(){
        return "Hello from HelloBean class :)";
    }

    public int getCount() {
        return ++count;
    }
}
